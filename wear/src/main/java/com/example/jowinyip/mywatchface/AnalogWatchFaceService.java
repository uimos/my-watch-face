package com.example.jowinyip.mywatchface;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.wearable.watchface.CanvasWatchFaceService;
import android.support.wearable.watchface.WatchFaceStyle;
import android.util.Log;
import android.view.SurfaceHolder;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.DataItemBuffer;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.Wearable;

import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by jowin on 4/9/16.
 */
public class AnalogWatchFaceService extends CanvasWatchFaceService {

    private static final String TAG = "AnalogWatchFaceService";

    @Override
    public Engine onCreateEngine() {
        return new WatchFaceEngine();
    }

    private class WatchFaceEngine extends Engine implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

        private GoogleApiClient mGoogleApiClient;

        private Typeface WATCH_TEXT_TYPEFACE = Typeface.createFromAsset( getAssets(), "fonts/Montserrat-Black.otf");

        static final int MSG_UPDATE_TIME = 42;
        static final int INTERACTIVE_UPDATE_RATE_MS = 1000;

        boolean mRegisteredTimeZoneReceiver = false;
        boolean mLowBitAmbient;
        boolean mBurnInProtection;

        Calendar mCalendar;

        Paint mBackgroundPaint;
        Paint mHourPaint;
        Paint mMinutePaint;
        Paint mSecondPaint;

        Paint mTickPaint;
        Paint mTextColorPaint;

        final Handler mUpdateTimeHandler = new Handler() {
            @Override
            public void handleMessage(Message message) {
                switch (message.what) {
                    case MSG_UPDATE_TIME:
                        invalidate();
                        if (shouldTimerBeRunning()) {
                            long timeMs = System.currentTimeMillis();
                            long delayMs = INTERACTIVE_UPDATE_RATE_MS
                                    - (timeMs % INTERACTIVE_UPDATE_RATE_MS);
                            mUpdateTimeHandler.sendEmptyMessageDelayed(MSG_UPDATE_TIME, delayMs);
                        }
                        break;
                }
            }
        };

        final BroadcastReceiver mTimeZoneReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                mCalendar.setTimeZone(TimeZone.getDefault());
            }
        };

        private boolean shouldTimerBeRunning() {
            return isVisible() && !isInAmbientMode();
        }

        private void registerReceiver() {
            if (mRegisteredTimeZoneReceiver) {
                return;
            }
            mRegisteredTimeZoneReceiver = true;
            IntentFilter filter = new IntentFilter(Intent.ACTION_TIMEZONE_CHANGED);
            AnalogWatchFaceService.this.registerReceiver(mTimeZoneReceiver, filter);
        }

        private void unregisterReceiver() {
            if (!mRegisteredTimeZoneReceiver) {
                return;
            }
            mRegisteredTimeZoneReceiver = false;
            AnalogWatchFaceService.this.unregisterReceiver(mTimeZoneReceiver);
        }

        private void releaseGoogleApiClient() {
            if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
                Wearable.DataApi.removeListener(mGoogleApiClient,
                        onDataChangedListener);
                mGoogleApiClient.disconnect();
            }
        }

        private void drawTimeText( Canvas canvas, Rect bounds ) {
            int day = mCalendar.get(Calendar.DAY_OF_WEEK);
            int date = mCalendar.get(Calendar.DATE);
            String mday = "", dateText;

            int width = bounds.width();
            int height = bounds.height();

            switch (day){
                case 1:
                    mday = "SUN";
                    break;
                case 2:
                    mday = "MON";
                    break;
                case 3:
                    mday = "TUE";
                    break;
                case 4:
                    mday = "WED";
                    break;
                case 5:
                    mday = "THU";
                    break;
                case 6:
                    mday = "FRI";
                    break;
                case 7:
                    mday = "SAT";
                    break;
            }

            dateText = mday + " " + Integer.toString(date);

            mTextColorPaint = new Paint();
            mTextColorPaint.setARGB( 255, 236, 240, 241);
            mTextColorPaint.setTypeface( WATCH_TEXT_TYPEFACE);
            canvas.drawText( dateText, width/1.5f, height/2.0f, mTextColorPaint );
        }

        //Utility methods
        private void initBackground(Canvas canvas, Rect bounds) {
            // Constant to help calculate clock hand rotations
            final float TWO_PI = (float) Math.PI * 2f;

            int width = bounds.width();
            int height = bounds.height();

            // Find the center. Ignore the window insets so that, on round watches
            // with a "chin", the watch face is centered on the entire screen, not
            // just the usable portion.
            float centerX = width / 2f;
            float centerY = height / 2f;

            float layoutNeedleLength = centerX;

            for(float i = 0f; i < 12f; i++) {
                float needleX = (float) Math.sin(i/12.0f * TWO_PI) * layoutNeedleLength;
                float needleY = (float) -Math.cos(i/12.0f * TWO_PI) * layoutNeedleLength;
                float needleX1 = (float) Math.sin(i/12.0f * TWO_PI) * layoutNeedleLength * 0.7f;
                float needleY1 = (float) -Math.cos(i/12.0f * TWO_PI) * layoutNeedleLength * 0.7f;

                if(i % 3.0f == 0.0f) {
                    canvas.drawLine(centerX + needleX1, centerY + needleY1, centerX + needleX, centerY + needleY, mHourPaint);
                } else {
                    canvas.drawLine(centerX + needleX1, centerY + needleY1, centerX + needleX, centerY + needleY, mSecondPaint);
                }
            }
        }

        private void updateUiForConfigDataMap(final DataMap config) {
            boolean uiUpdated = false;
            for (String configKey : config.keySet()) {
                if (!config.containsKey(configKey)) {
                    continue;
                }
                int color = config.getInt(configKey);
                if (Log.isLoggable(TAG, Log.DEBUG)) {
                    Log.d(TAG, "Found watch face config key: " + configKey + " -> "
                            + Integer.toHexString(color));
                }
                if (updateUiForKey(configKey, color)) {
                    uiUpdated = true;
                }
            }
            if (uiUpdated) {
                invalidate();
            }
        }

        private boolean updateUiForKey(String configKey, int color) {
            if (configKey.equals(AnalogWatchFaceUtil.KEY_BACKGROUND_COLOR)) {
                setInteractiveBackgroundColor(color);
            } else {
                Log.w(TAG, "Ignoring unknown config key: " + configKey);
                return false;
            }
            return true;
        }

        private void setInteractiveBackgroundColor(int color) {
            updatePaintIfInteractive(mSecondPaint, color);
        }

        private void updatePaintIfInteractive(Paint paint, int interactiveColor) {
            if (!isInAmbientMode() && paint != null) {
                paint.setColor(interactiveColor);
            }
        }

        private void setDefaultValuesForMissingConfigKeys(DataMap config) {
            addIntKeyIfMissing(config, AnalogWatchFaceUtil.KEY_BACKGROUND_COLOR,
                    AnalogWatchFaceUtil.COLOR_VALUE_DEFAULT_AND_AMBIENT_BACKGROUND);
        }

        private void addIntKeyIfMissing(DataMap config, String key, int color) {
            if (!config.containsKey(key)) {
                config.putInt(key, color);
            }
        }

        private void updateParamsForDataItem(DataItem item) {
            if ((item.getUri().getPath()).equals("/activity_analog_watch_face_config")) {
                DataMap dataMap = DataMapItem.fromDataItem(item).getDataMap();
                if (dataMap.containsKey("text_color")) {
                    int tc = dataMap.getInt("text_color");
                    mBackgroundPaint.setColor(tc);
                    invalidate();
                }
            }
        }

        private final DataApi.DataListener onDataChangedListener = new DataApi.DataListener() {
            @Override
            public void onDataChanged(DataEventBuffer dataEvents) {
                for (DataEvent event : dataEvents) {
                    if (event.getType() == DataEvent.TYPE_CHANGED) {
                        DataItem item = event.getDataItem();
                        updateParamsForDataItem(item);
                    }
                }

                dataEvents.release();
                if (isVisible() && !isInAmbientMode()) {
                    invalidate();
                }
            }
        };

        private final ResultCallback<DataItemBuffer> onConnectedResultCallback = new ResultCallback<DataItemBuffer>() {
            @Override
            public void onResult(DataItemBuffer dataItems) {
                for (DataItem item : dataItems) {
                    updateParamsForDataItem(item);
                }

                dataItems.release();
                if (isVisible() && !isInAmbientMode()) {
                    invalidate();
                }
            }
        };

        //Overridden methods
        @Override
        public void onCreate(SurfaceHolder holder) {
            super.onCreate(holder);

            setWatchFaceStyle( new WatchFaceStyle.Builder( AnalogWatchFaceService.this )
                    .setBackgroundVisibility( WatchFaceStyle.BACKGROUND_VISIBILITY_INTERRUPTIVE )
                    .setCardPeekMode( WatchFaceStyle.PEEK_MODE_VARIABLE )
                    .setShowSystemUiTime( false )
                    .build()
            );

            mGoogleApiClient = new GoogleApiClient.Builder(AnalogWatchFaceService.this)
                    .addApi(Wearable.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();

            mGoogleApiClient.connect();

            mHourPaint = new Paint();
            mHourPaint.setARGB( 255, 236, 240, 241);
            mHourPaint.setStrokeWidth(8.0f);
            mHourPaint.setAntiAlias(true);
            mHourPaint.setStrokeCap(Paint.Cap.SQUARE);

            mMinutePaint = new Paint();
            mMinutePaint.setARGB( 255, 236, 240, 241);
            mMinutePaint.setStrokeWidth(5.0f);
            mMinutePaint.setAntiAlias(true);
            mMinutePaint.setStrokeCap(Paint.Cap.SQUARE);

            mSecondPaint = new Paint();
            mSecondPaint.setARGB( 255, 236, 240, 241);
            mSecondPaint.setStrokeWidth(3.0f);
            mSecondPaint.setAntiAlias(true);
            mSecondPaint.setStrokeCap(Paint.Cap.SQUARE);

            mTickPaint = new Paint();
            mTickPaint.setARGB( 255, 236, 240, 241);
            mTickPaint.setStrokeWidth(5.0f);
            mTickPaint.setAntiAlias(true);
            mTickPaint.setStrokeCap(Paint.Cap.SQUARE);

            mBackgroundPaint = new Paint();
            mBackgroundPaint.setARGB(255, 0, 0, 0);


            mCalendar = Calendar.getInstance();

            //initDisplayText();
        }

        @Override
        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);

            if (visible) {
                mGoogleApiClient.connect();
                registerReceiver();
                mCalendar.setTimeZone(TimeZone.getDefault());


            } else {
                unregisterReceiver();

                releaseGoogleApiClient();
            }

            updateTimer();
        }

        @Override
        public void onPropertiesChanged(Bundle properties) {
            super.onPropertiesChanged(properties);
            mLowBitAmbient = properties.getBoolean(PROPERTY_LOW_BIT_AMBIENT, false);
            mBurnInProtection = properties.getBoolean(PROPERTY_BURN_IN_PROTECTION,
                    false);
        }

        @Override
        public void onTimeTick() {
            super.onTimeTick();

            invalidate();
        }

        @Override
        public void onAmbientModeChanged(boolean inAmbientMode) {

            super.onAmbientModeChanged(inAmbientMode);

            if (mLowBitAmbient) {
                boolean antiAlias = !inAmbientMode;
                mHourPaint.setAntiAlias(antiAlias);
                mMinutePaint.setAntiAlias(antiAlias);
                mSecondPaint.setAntiAlias(antiAlias);
                mTickPaint.setAntiAlias(antiAlias);
            }
            invalidate();
            updateTimer();
        }

        @Override
        public void onDraw(Canvas canvas, Rect bounds) {
            super.onDraw(canvas, bounds);

            canvas.drawColor(0, PorterDuff.Mode.CLEAR);
            if (!isInAmbientMode()) {
                mBackgroundPaint.setARGB(192, 44, 62, 80);
                canvas.drawRect(0, 0, bounds.width(), bounds.height(), mBackgroundPaint);
            } else {
                mBackgroundPaint.setARGB(255, 0, 0, 0);
                canvas.drawRect(0, 0, bounds.width(), bounds.height(), mBackgroundPaint);
            }
            initBackground(canvas, bounds);

            mCalendar = Calendar.getInstance();

            drawTimeText( canvas, bounds);

            // Constant to help calculate clock hand rotations
            final float TWO_PI = (float) Math.PI * 2f;

            int width = bounds.width();
            int height = bounds.height();

            // Find the center. Ignore the window insets so that, on round watches
            // with a "chin", the watch face is centered on the entire screen, not
            // just the usable portion.
            float centerX = width / 2f;
            float centerY = height / 2f;

            // Compute rotations and lengths for the clock hands.
            float seconds = mCalendar.get(Calendar.SECOND) +
                    mCalendar.get(Calendar.MILLISECOND) / 1000f;
            float secRot = seconds / 60f * TWO_PI;
            float minutes = mCalendar.get(Calendar.MINUTE) + seconds / 60f;
            float minRot = minutes / 60f * TWO_PI;
            float hours = mCalendar.get(Calendar.HOUR) + minutes / 60f;
            float hrRot = hours / 12f * TWO_PI;

            float secLength = centerX - 20;
            float minLength = centerX - 20;
            float hrLength = centerX - 80;

            // Only draw the second hand in interactive mode.
            if (!isInAmbientMode()) {
                float secX = (float) Math.sin(secRot) * secLength;
                float secY = (float) -Math.cos(secRot) * secLength;
                float secX1 = (float) Math.sin(secRot) * 30;
                float secY1 = (float) -Math.cos(secRot) * 30;
                canvas.drawLine(centerX + secX1, centerY + secY1, centerX + secX, centerY +
                        secY, mSecondPaint);
            }

            // Draw the minute and hour hands.
            float minX = (float) Math.sin(minRot) * minLength;
            float minY = (float) -Math.cos(minRot) * minLength;
            float minX1 = (float) Math.sin(minRot) * 30;
            float minY1 = (float) -Math.cos(minRot) * 30;
            canvas.drawLine(centerX + minX1, centerX + minY1, centerX + minX, centerY + minY,
                    mMinutePaint);
            float hrX = (float) Math.sin(hrRot) * hrLength;
            float hrY = (float) -Math.cos(hrRot) * hrLength;
            float hrX1 = (float) Math.sin(hrRot) * 30;
            float hrY1 = (float) -Math.cos(hrRot) * 30;
            canvas.drawLine(centerX + hrX1, centerY + hrY1, centerX + hrX, centerY + hrY,
                    mHourPaint);
        }

        private void updateTimer() {
            mUpdateTimeHandler.removeMessages( MSG_UPDATE_TIME );
            if( isVisible() && !isInAmbientMode() ) {
                mUpdateTimeHandler.sendEmptyMessage( MSG_UPDATE_TIME );
            }
        }

        @Override
        public void onConnected(Bundle bundle) {
            Wearable.DataApi.addListener(mGoogleApiClient,
                    onDataChangedListener);
            Wearable.DataApi.getDataItems(mGoogleApiClient).
                    setResultCallback(onConnectedResultCallback);
        }

        @Override  // GoogleApiClient.ConnectionCallbacks
        public void onConnectionSuspended(int cause) {
            if (Log.isLoggable(TAG, Log.DEBUG)) {
                Log.d(TAG, "onConnectionSuspended: " + cause);
            }
        }

        @Override  // GoogleApiClient.OnConnectionFailedListener
        public void onConnectionFailed(ConnectionResult result) {
            if (Log.isLoggable(TAG, Log.DEBUG)) {
                Log.d(TAG, "onConnectionFailed: " + result);
            }
        }

        @Override
        public void onDestroy() {
            mUpdateTimeHandler.removeMessages(MSG_UPDATE_TIME);
            releaseGoogleApiClient();
            super.onDestroy();
        }
    }

}
